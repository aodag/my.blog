import os
from mako.lookup import TemplateLookup
from webob.dec import wsgify
from . import models


here = os.path.dirname(__file__)
templates = TemplateLookup(
    directories=[os.path.join(here, "templates")])


@wsgify
def index(request):
    blog = models.Blog.query.filter(models.Blog.name == "default").one()
    tmpl = templates.get_template("index.html")
    return tmpl.render(title="Hello, world!",
                       blog=blog)
