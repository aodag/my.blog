from sqlalchemy import (
    Column,
    Integer,
    Unicode,
    UnicodeText,
)
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
)
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
DBSession = scoped_session(
    sessionmaker())


def init(engine):
    DBSession.remove()
    DBSession.configure(bind=engine)


class Blog(Base):
    __tablename__ = 'blogs'
    query = DBSession.query_property()
    id = Column(Integer, primary_key=True)
    name = Column(Unicode, unique=True)
    title = Column(UnicodeText)
