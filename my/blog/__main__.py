import waitress
from . import make_app


def main():
    app = make_app({})
    host = "0.0.0.0"
    port = 5000
    waitress.serve(app,
                   host=host,
                   port=port)


if __name__ == '__main__':
    main()
