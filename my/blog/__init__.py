from repoze.tm import TM
from sqlalchemy import engine_from_config
from . import models
from . import views


def make_app(global_conf, **app_conf):
    engine = engine_from_config(app_conf)
    models.init(engine)
    app = views.index
    app = TM(app)
    return app
