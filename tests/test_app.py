import pytest
import webtest


app_conf = {
    "sqlalchemy.url": 'postgresql://myblog:myblog@localhost/myblog_test',
}


@pytest.fixture
def app():
    from my.blog import make_app
    app = make_app({}, **app_conf)
    app = webtest.TestApp(app)
    return app


@pytest.fixture(autouse=True)
def create_all(request, app):
    from my.blog import models
    models.Base.metadata.drop_all(
        bind=models.DBSession.bind)
    models.Base.metadata.create_all(
        bind=models.DBSession.bind)


def test_index(app):
    import transaction
    from my.blog import models
    with transaction.manager:
        blog = models.Blog(name="default",
                           title="test blog")
        models.DBSession.add(blog)
    result = app.get("/")
    assert "test blog" in result
